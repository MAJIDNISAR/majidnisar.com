---
layout: post
title: About me
subtitle: Why you'd want to go on a date with me
bigimg:me.jpg
---

My name is Majid Nisar. My experiences have taught a lot in the journey called life:

- I am working as Project Manager Eresolute Consultancy and services
- I'm a guest faculty for computer science at Islamic University of Science & Technology
- I have worked as an Assistant Professor of Computer Science and Information Technology for more than 5 years at Islamic University of Science & Technology

What else do you need?

### My Other Site

To be honest, I'm having some trouble remembering right now, so why don't you just watch [my wordpress site](http://majidnisar.wordpress.com) and it will answer **all** your questions.
![m'lady](/img/Me.jpg)
